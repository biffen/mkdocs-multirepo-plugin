.POSIX:
.SUFFIXES:

SHELL                           = /bin/sh

BLACK                           = black
BLACK_FLAGS                     =
MODULE                          = mkdocs-multirepo-plugin
PIP                             = pip
PIP_FLAGS                       =
PIP_INSTALL_FLAGS               =
PIP_UNINSTALL_FLAGS             =
PYLINT                          = pylint
PYLINT_FLAGS                    =
PYTEST                          = pytest
PYTEST_FLAGS                    =
PYTHON                          = python
PYTHON_FLAGS                    =

-include config.user.make

all: check

build:
	$(PYTHON) $(PYTHON_FLAGS) setup.py build

check: lint test
	$(BLACK) $(BLACK_FLAGS) --check .
	$(PYTHON) $(PYTHON_FLAGS) setup.py check

clean: mostlyclean

dist:
	$(PYTHON) $(PYTHON_FLAGS) setup.py sdist

format:
	$(BLACK) $(BLACK_FLAGS) .

install:
	$(PIP) $(PIP_FLAGS) install $(PIP_INSTALL_FLAGS) .

lint:
	$(PYLINT) $(PYLINT_FLAGS) $(shell find . -name '*.py')

mostlyclean:
	$(PYTHON) $(PYTHON_FLAGS) setup.py clean

print-%:
	$(info $*=$($*))
	@:

publish: check dist
	$(PYTHON) $(PYTHON_FLAGS) setup.py sdist

test:
	$(PYTEST) $(PYTEST_FLAGS)

uninstall:
	$(PIP) $(PIP_FLAGS) uninstall $(PIP_UNINSTALL_FLAGS) $(MODULE)

.PHONY: all build check clean dist format install lint mostlyclean print-%
.PHONY: publish test uninstall
