# MkDocs Multi-Repo Plugin #

A plugin for MkDocs that clones a number of Git repositories to create a single
website from the Markdown files in all of the repositories.

## Usage ##

In your `mkdocs.yml`:

```yaml

# ...

plugins:
  - multirepo:
      repos:
        https://example.com/git/repository.git:
        https://example.com/git/other-repository.git:
          branch: dev

```

Then running `mkdocs build` as usual will clone the repositories into the `docs`
directory (or whatever it’s configured to be) before MkDocs generates the site.

Each time the site is built the repositories will be synced with their remotes.
`mkdocs server` etc. also work as expected.

## Configuration ##

`plugins/multirepo` takes the following configuration:

### `repos` ###

A map of repositories and their (optional) individual configuration:

#### `branch` ###

The branch to check out, if not the default.

#### `name` ###

A display name to use in `index.md`. Defaults to the path.

#### `path` ###

A path whither to clone. Default is the path part of the URL, with any trailing
`.git` removed.

#### `url` ###

Specifies the URL whence to clone. If not set then the key is used (like in the
example above).

  [MkDocs]: https://www.mkdocs.org
