#!/usr/bin/env python3
# -*- coding: utf-8 -*-

u"""Tests for ``repository.py``."""

import unittest

from multirepo.repository import Repository


class TestRepository(unittest.TestCase):
    u"""Tests for ``repository.Repository``."""

    def setUp(self):
        self.repo = Repository("docs", "git://example.com/foo/bar.git", {})

    def test_clone_dir(self):
        u"""Test for ``clone_dir()``."""
        self.assertEqual(self.repo.clone_dir(), "docs/example.com/foo/bar")

    def test_git_dir(self):
        u"""Test for ``git_dir()``."""
        self.assertEqual(self.repo.git_dir(), ".multirepo/example.com/foo/bar")


if __name__ == "__main__":
    unittest.main()
