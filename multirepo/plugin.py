#!/usr/bin/env python3
# -*- coding: utf-8 -*-


u"""A plugin for MkDocs."""

import os

from tqdm import tqdm
import mkdocs

from . import repository

serving = False  # pylint: disable=invalid-name


class MultiRepo(mkdocs.plugins.BasePlugin):
    u"""A plugin for MkDocs."""

    config_scheme = {("repos", mkdocs.config.config_options.Type(dict, default={}))}
    repos = None

    def on_serve(self, *_args, **_kwargs):  # pylint: disable=no-self-use
        u"""Called when MkDocs serves the site."""

        global serving  # pylint: disable=global-statement,invalid-name
        serving = True

    def on_pre_build(self, config):
        u"""Called before MkDocs builds."""

        if serving:
            return

        docs_dir = config["docs_dir"]
        assert docs_dir

        if self.repos is None:
            self.repos = [
                repository.Repository(docs_dir, key, data)
                for key, data in self.config["repos"].items()
            ]
            self.repos.sort()

        for repo in tqdm(self.repos):
            repo.sync()

        with open(os.path.join(docs_dir, "index.md"), "w") as index:
            index.write(
                "# %s\n\n" % config["site_name"] if "site_name" in config else "TODO"
            )

            if "site_description" in config and config["site_description"] is not None:
                index.write("%s\n\n" % config["site_description"])

            for repo in self.repos:
                index.write("*   [%s](%s)\n" % (repo.name(), repo.path()))
