#!/usr/bin/env python3
# -*- coding: utf-8 -*-

u"""A clone of a repository."""

from urllib.parse import urlparse
import os
import re
import shutil
import subprocess


class Repository:
    u"""A clone of a repository."""

    def __init__(self, docs_dir, key, data):
        self.docs_dir = docs_dir
        self.key = key
        self.data = data or {}
        self.data.setdefault("url", self.key)

        url = urlparse(self.data["url"])
        self.data.setdefault("scheme", url.scheme)

        match = re.match(
            r"^(?:(?P<user>.+?)(?::(?P<password>.*?))?@)?(?P<domain>.+?)(?::(?P<port>\d+))?$",
            url.netloc,
        )
        assert match

        self.data.setdefault(
            "path", match.group("domain") + re.sub(r"\.git$", "", url.path)
        )

        self.data.setdefault("name", self.data["path"])

    def __lt__(self, other):
        return self.path() < other.path()

    def clone(self):
        u"""Creates a local clone of the repository."""
        clone_dir = self.clone_dir()

        if os.path.exists(clone_dir):
            remote_url = (
                subprocess.run(
                    ["git", "-C", clone_dir, "config", "--get", "remote.origin.url"],
                    stdout=subprocess.PIPE,
                )
                .stdout.decode("utf-8")
                .strip()
            )
            if remote_url == self.data["url"]:
                return

        self.remove()

        os.makedirs(os.path.dirname(self.git_dir()), exist_ok=True)
        clone_cmd = ["git", "clone", "--separate-git-dir", self.git_dir()]
        if "branch" in self.data:
            clone_cmd.extend(["--branch", self.data["branch"]])
        clone_cmd.extend(["--", self.data["url"], clone_dir])
        subprocess.run(clone_cmd, check=True)

    def clone_dir(self):
        u"""The file system path whither to clone."""
        return os.path.join(self.docs_dir, self.path())

    def git_dir(self):
        u"""The separate Git directory."""
        return os.path.join(os.path.dirname(self.docs_dir), ".multirepo", self.path())

    def name(self):
        u"""A name for the repository, for human consumption."""
        return self.data["name"]

    def path(self):
        u"""The relative path to the clone."""
        return self.data["path"]

    def remove(self):
        u"""Removes the clone from disk."""
        for path in [self.clone_dir(), self.git_dir()]:
            if os.path.exists(path):
                shutil.rmtree(path)

    def sync(self):
        u"""Makes sure there’s a local clone that is in sync with the remote repository.

        If there’s no clone it will be created.

        Then it will be synced with the remote."""
        self.clone()

        clone_dir = self.clone_dir()

        subprocess.run(["git", "-C", clone_dir, "clean", "-dfx"], check=True)
        subprocess.run(["git", "-C", clone_dir, "fetch", "--prune"], check=True)

        status = subprocess.run(
            ["git", "-C", clone_dir, "status", "--branch", "--porcelain=v1"],
            stdout=subprocess.PIPE,
        ).stdout.decode("utf-8")
        if re.match(r"^##.*\bbehind\b", status):
            subprocess.run(["git", "-C", clone_dir, "rebase"], check=True)
