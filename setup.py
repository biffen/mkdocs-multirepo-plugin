#!/usr/bin/env python3
# -*- coding: utf-8 -*-

u"""Setup for ``mkdocs-multirepo-plugin``."""

from setuptools import find_packages, setup

setup(
    name="mkdocs-multirepo-plugin",
    version="0.0",
    description="TODO",
    url="TODO",
    author="TODO",
    author_email="TODO",
    license="MIT",
    packages=find_packages(exclude=["*.tests"]),
    entry_points={"mkdocs.plugins": ["multirepo = multirepo.plugin:MultiRepo"]},
    keywords="TODO",
    install_requires=["mkdocs", "tqdm"],
    include_package_data=True,
    zip_safe=False,
)
